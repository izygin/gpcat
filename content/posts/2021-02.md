---
title: "2021-02. Pirelli Gran Premio del Made In Italy e dell'Emilia Romagna"
date: 2021-04-18T14:06:22+03:00
draft: false
---

## Гран-при Эмилии-Романьи

Автодром Энцо и Дино Феррари, Имола, Италия

{{< video "https://storage.yandexcloud.net/gpcat/2021_02.mp4" >}}
[Link](https://storage.yandexcloud.net/gpcat/2021_02.mp4)