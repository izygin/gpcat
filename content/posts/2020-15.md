---
title: "2020-15. Gulf Air Bahrain Grand Prix"
date: 2020-11-29T14:06:22+03:00
draft: false
---

## Гран-при Бахрейна

Международный автодром Бахрейна, Сахир, Бахрейн

{{< video "https://storage.yandexcloud.net/gpcat/2020_15.mp4" >}}
[Link](https://storage.yandexcloud.net/gpcat/2020_15.mp4)