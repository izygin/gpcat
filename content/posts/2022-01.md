---
title: "2022-01. Gulf Air Bahrain Grand Prix"
date: 2022-03-20T14:06:22+03:00
draft: false
---

## Гран-при Бахрейна

Международный автодром Бахрейна, Сахир, Бахрейн

{{< video "https://storage.yandexcloud.net/gpcat/2022_01.mp4" >}}
[Link](https://storage.yandexcloud.net/gpcat/2022_01.mp4)